const express = require("express");
const { index } = require("../controllers/index.controller");
const { game } = require("../controllers/play.controller");
const { dashboard } = require("../controllers/dashboard/dashboard.controller");
const { logout } = require("../controllers/login.controller");
const { wrongUrl } = require("../controllers/errors/error.controller");
const registrationRoutes = require("./user/registration.routes");
const loginRoutes = require("./user/login.routes");
const historyRoutes = require("./dashboard/gamehistory.routes")
const bioDataRoutes = require("./dashboard/biodata.routes")


const { auth, guest } = require('../utils/method/method');

const router = express.Router();


router.get("/", index);
router.use("/play", auth, game);
router.use("/registration", guest, registrationRoutes);
router.use("/login", guest, loginRoutes);
router.get("/logout", logout);
router.get("/dashboard", auth, dashboard);
router.use("/history", auth, historyRoutes);
router.use("/biodata", auth, bioDataRoutes);


router.get("*", wrongUrl);


module.exports = router;
