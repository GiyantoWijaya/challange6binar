const bcrypt = require('bcrypt');
const fs = require('fs');
const saltRounds = 10;
const { sequelize, User, UserGameBiodata, UserGameHistory } = require('../../models');

const jwt = require('jsonwebtoken');
const { result } = require('lodash');

async function play() {
  await UserGameBiodata.create({
    "userId": "1",
    "name": "giyanto",
    "age": "16",
    "address": "jl.patimura",
    "about": "i love playing basketball"
  })
}
// play()
async function history() {
  await UserGameHistory.create({
    "userId": "1",
    "result": "giyanto",
    "computer": "asdasdasd",
    "player": "jl.patimura"
  })
}
// history()

// load all data users
async function loadUsers() {
  const showAll = await User.findAll()
  return showAll
  // const fileBuffer = fs.readFileSync('users.json', 'utf-8');
  // const users = JSON.parse(fileBuffer);
  // return users;
}
// save data users
function saveData(users) {
  fs.writeFileSync('users.json', JSON.stringify(users));
}

// add data users
function addUser(user) {
  const users = loadUsers();
  users.push(user);
  saveData(users);
}

// findUsers
async function findUser(email) {
  const user = await User.findOne({ where: { email } });
  return user
}

// encrypt password
function encrypt(password) {
  const result = bcrypt.hashSync(password, saltRounds);
  return result
}


// authenticator
const maxAge = 3 * 24 * 60 * 60;
function createToken(id) {
  return jwt.sign({ id }, 'secret', { expiresIn: maxAge })
}

async function authentication(email, password) {
  const user = await findUser(email);
  if (user) {
    const hasil = await bcrypt.compare(password, user.password);
    return hasil
  } else {
    console.log('akun tidak di temukan')
  }
}

function auth(req, res, next) {
  const token = req.cookies.jwt;

  if (token) {
    jwt.verify(token, 'secret', (err, decodedToken) => {
      if (err) {
        console.log(err.message);
        res.redirect('/login');
      } else {
        req.userId = decodedToken.id
        next();
      }
    });
  } else {
    res.redirect('/login');
  }
};

function guest(req, res, next) {
  const token = req.cookies.jwt;
  if (token) {
    res.redirect("/404")
  }
  next()
}


module.exports = { loadUsers, findUser, addUser, encrypt, authentication, auth, createToken, guest }