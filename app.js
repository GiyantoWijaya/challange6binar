const { urlencoded } = require('express');
const express = require('express');
const app = express();
const port = 3000;
const expressLayouts = require('express-ejs-layouts');
const session = require('express-session');
const flash = require('connect-flash');
const cookieParser = require('cookie-parser');
const { sequelize } = require('./models');
const main = { router } = require("./routes/routes");
const bodyParser = require('body-parser');
const methodOverride = require('method-override')

// view engine ejs
app.set('view engine', 'ejs');
// file static
app.use(express.static('public'));
// layouting
app.use(expressLayouts);
app.use(express.urlencoded({ extended: true }));
app.use(bodyParser.urlencoded({ extended: false }));
// session cookie
app.use(cookieParser());
app.use(session({
  cookie: { maxAge: 6000 },
  secret: 'secret',
  resave: true,
  saveUninitialized: true,
}));
// flash message
app.use(flash());
// // override method PATCH, PUT, DELETE
app.use(methodOverride('_method'))

app.use("/", main);

// API JSON
// app.get('/users', (req, res) => {
//   const data = loadUsers();
//   res.json(data);
// })

// start server
app.listen(port, async () => {
  console.log(`Example app listening on port ${port}`);
  try {
    await sequelize.authenticate();
    console.log('Connection database has been established successfully.');
  } catch (error) {
    console.error('Unable to connect to the database:', error);
  }
});