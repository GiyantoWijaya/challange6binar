exports.game = (req, res) => {
  res.status(200).render('play', {
    layout: 'play',
    title: 'Rock, Papper, Scissor',
  });
};