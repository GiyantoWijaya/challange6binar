const { User } = require('../../models');

exports.dashboard = async (req, res) => {
  const id = req.userId
  const pengguna = await User.findOne({ where: { id } })
  res.status(202).render(`dashboard/dashboard`, {
    layout: 'layouts/dashboard-layout',
    title: 'Challange Chapter 6',
    pengguna
  });
};